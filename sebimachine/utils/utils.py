#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

"""
===

MIT License

Copyright (c) 2018 Dusty.P https://github.com/dustinpianalto

Permission is hereby granted, free of charge, to any person obtaining
a copy of this software and associated documentation files (the
"Software"), to deal in the Software without restriction, including
without limitation the rights to use, copy, modify, merge, publish,
distribute, sublicense, and/or sell copies of the Software, and to
permit persons to whom the Software is furnished to do so, subject to
the following conditions:

The above copyright notice and this permission notice shall be
included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

"""


from io import StringIO
import sys
import asyncio
import discord
from discord.ext.commands.formatter import Paginator
from discord.ext import commands

from libneko.pag import navigator


class Capturing(list):
    def __enter__(self):
        self._stdout = sys.stdout
        sys.stdout = self._stringio = StringIO()
        return self

    def __exit__(self, *args):
        self.extend(self._stringio.getvalue().splitlines())
        del self._stringio    # free up some memory
        sys.stdout = self._stdout


def to_list_of_str(items, out: list=list(), level=1, recurse=0):
    def rec_loop(item, key, out, level):
        quote = '"'
        if type(item) == list:
            out.append(f'{"    "*level}{quote+key+quote+": " if key else ""}[')
            new_level = level + 1
            out = to_list_of_str(item, out, new_level, 1)
            out.append(f'{"    "*level}]')
        elif type(item) == dict:
            out.append(f'{"    "*level}{quote+key+quote+": " if key else ""}{{')
            new_level = level + 1
            out = to_list_of_str(item, out, new_level, 1)
            out.append(f'{"    "*level}}}')
        else:
            out.append(f'{"    "*level}{quote+key+quote+": " if key else ""}{repr(item)},')

    if type(items) == list:
        if not recurse:
            out = list()
            out.append('[')
        for item in items:
            rec_loop(item, None, out, level)
        if not recurse:
            out.append(']')
    elif type(items) == dict:
        if not recurse:
            out = list()
            out.append('{')
        for key in items:
            rec_loop(items[key], key, out, level)
        if not recurse:
            out.append('}')

    return out


def paginate(text, maxlen=1990):
    paginator = Paginator(prefix='```py', max_size=maxlen+10)
    if type(text) == list:
        data = to_list_of_str(text)
    elif type(text) == dict:
        data = to_list_of_str(text)
    else:
        data = str(text).split('\n')
    for line in data:
        if len(line) > maxlen:
            n = maxlen
            for l in [line[i:i+n] for i in range(0, len(line), n)]:
                paginator.add_line(l)
        else:
            paginator.add_line(line)
    return paginator.pages


async def run_command(args):
    # Create subprocess
    process = await asyncio.create_subprocess_shell(
        args,
        # stdout must a pipe to be accessible as process.stdout
        stdout=asyncio.subprocess.PIPE)
    # Wait for the subprocess to finish
    stdout, stderr = await process.communicate()
    # Return stdout
    return stdout.decode().strip()


def rand_hex():
    """Creates a random colour."""
    from random import randint
    return randint(0, 0xFFFFFF)


def _sync_pretty_list(iterable, sep, to_s):
    return sep.join(map(to_s, iterable))


async def _async_pretty_list(async_iterable, sep, to_s):
    iterable = []
    async for item in async_iterable:
        iterable.append(item)
    return _sync_pretty_list(iterable, sep, to_s)
    

def pretty_list(iterable, *, sep, to_s=str):
    if hasattr(iterable, '__anext__'):
        return _async_pretty_list(iterable, sep, to_s)
    else:
        return _sync_pretty_list(iterable, sep, to_s)


async def embed_dm_or_guild(ctx, embed_list: list):
    """Starts a embed navigation session or sends each embed seperatly"""
    if ctx.guild:
        navigator.EmbedNavigator(pages=embed_list, ctx=ctx).start()
    else:
        await asyncio.gather(*[ctx.send(embed=em) for em in embed_list])


class NotContributor(commands.CommandError):
    pass


def is_contributor_check(ctx):
    if ctx.author.id in ctx.bot.ownerlist:
        return True
    raise NotContributor(f"{ctx.author} is not a contributor!")


def is_contributor():
    def decorator(ctx):
        is_contributor_check(ctx)
    return commands.check(decorator)


class NotStaff(commands.CommandError):
    pass


def is_staff_check(ctx):
    if discord.utils.get(ctx.guild.roles, id=477417027913515020) in ctx.author.roles:
        return True
    raise NotStaff(f"{ctx.author} is not a staff member!")


def is_staff():
    def decorator(ctx):
        is_staff_check(ctx)
    return commands.check(decorator)
