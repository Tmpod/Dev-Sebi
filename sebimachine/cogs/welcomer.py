#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
import discord
from discord.ext import commands
from libneko import embeds


class Welcomer:
    def __init__(self ,bot):
        self.bot = bot
        self.welcomer_ch = self.bot.get_channel(265828729970753537) # #general


    async def on_member_join(self, member: discord.Member):
        # raw_msg = ... # get the custom message from the db
        # formated_msg = raw_msg.replace(r"%%", member.guild.name).replace("&&", str(member)).replace("@@", member.mention) # format it
        formated_msg = f"**Welcome {member.mention}**"
        welcomer_ch = self.bot.get_channel(265828729970753537)
        em = embeds.Embed(
            description=formated_msg,
            colour=0x51E723) # Nice green
        em.set_author(
            name="New user joined!", 
            icon_url=member.guild.icon_url)
        em.set_footer(text=f"Member #{member.guild.member_count} in {member.guild.name}")
        em.set_thumbnail(url=member.avatar_url)
        em.add_field(
            name="Joined Discord on", 
            value=str(member.created_at))
        em.add_field(
            name="ID", 
            value=str(member.id))
        em.add_field(
            name="Type", 
            value=member.bot and "Bot" or "Human")
        await welcomer_ch.send(embed=em)




    async def on_member_remove(self, member: discord.Member):
        # raw_msg = ... # get the custom message from the db
        # formated_msg = raw_msg.replace(r"%%", member.guild.name).replace("&&", str(member)).replace("@@", member.mention) # format it
        formated_msg = f"**Welcome {member.mention}**"
        welcomer_ch = self.bot.get_channel(265828729970753537)            
        em = embeds.Embed(
            description=formated_msg,
            colour=0xFB3719) # Nice red-orange
        em.set_author(
            name="Member left!", 
            icon_url=member.guild.icon_url)
        em.set_footer(text=member.guild.name)
        em.set_thumbnail(url=member.avatar_url)
        em.add_field(
            name="Joined Discord on", 
            value=str(member.created_at))
        em.add_field(
            name="Joined this guild on", 
            value=str(member.joined_at))           
        em.add_field(
            name="ID", 
            value=str(member.id))
        em.add_field(
            name="Type", 
            value=member.bot and "Bot" or "Human")
        em.add_field(
            name="Current member count",
            value=str(member.guild.member_count))
        await welcomer_ch.send(embed=em)


def setup(bot):
    bot.add_cog(Welcomer(bot))