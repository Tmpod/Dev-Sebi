#!/usr/bin/python
# -*- coding: utf-8 -*-

from discord.ext import commands
import discord
import traceback
import aiofiles
import os
import shutil
from libneko import embeds
from sebimachine.utils.utils import is_contributor_check


class Contributors:
    """
    CogName should be the name of the cog
    """

    def __init__(self, bot):
        self.bot = bot


    def __local_check(self, ctx):
        return is_contributor_check(ctx)


    @commands.command()
    async def reload(self, ctx, *, extension: str):
        """Reload an extension."""
        await ctx.trigger_typing()
        # if ctx.author.id not in self.bot.ownerlist:
            # return await ctx.send(embed=embeds.Embed(
            #     description="Only my contributors can use me like this :blush:",
            #     colour=self.bot.error_color,
            #     timestamp=None), 
            #     delete_after=10
            #     )

        extension = extension.lower()
        try:
            self.bot.unload_extension("sebimachine.cogs.{}".format(extension))
            self.bot.load_extension("sebimachine.cogs.{}".format(extension))
        except Exception as e:
            traceback.print_exc()
            await ctx.send(embed=embeds.Embed(
                description=f"Could not reload `{extension}` -> `{e}`",
                colour=self.bot.error_color,
                timestamp=None))
        else:
            await ctx.send(embed=embeds.Embed(
                description=f"Reloaded `{extension}`.",
                colour=discord.Colour.green()))


    @commands.command()
    async def reloadall(self, ctx):
        """Reload all extensions."""
        await ctx.trigger_typing()
        try:
            for extension in self.bot.extensions:
                self.bot.unload_extension(extension)
                self.bot.load_extension(extension)
            await ctx.send(embed=embeds.Embed(
                description="Reloaded all!",
                colour=discord.Colour.green(),
                timestamp=None))
        except Exception as e:
            await ctx.send(embed=embeds.Embed(
                description=f"Could not reload `{extension}` -> `{e}`.",
                colour=self.bot.error_color,
                timestamp=None))


    @commands.command()
    async def unload(self, ctx, *, extension: str):
        """Unload an extension."""
        await ctx.trigger_typing()
        extension = extension.lower()
        try:
            self.bot.unload_extension("sebimachine.cogs.{}".format(extension))
            await ctx.send(embed=embeds.Embed(
                description=f"Unloaded `{extension}`.",
                colour=discord.Colour.green()))

        except Exception as e:
            traceback.print_exc()
            await ctx.send(embed=embeds.Embed(
                description=f"Could not unload `{extension}` -> `{e}`.",
                colour=self.bot.error_color,
                timestamp=None))

    @commands.command()
    async def load(self, ctx, *, extension: str):
        """Load an extension."""
        await ctx.trigger_typing()
        extension = extension.lower()
        try:
            self.bot.load_extension("sebimachine.cogs.{}".format(extension))
        except Exception as e:
            traceback.print_exc()
            await ctx.send(embed=embeds.Embed(
                description=f"Could not load `{extension}` -> `{e}`.",
                colour=self.bot.error_color,
                timestamp=None))
        else:
            await ctx.send(embed=embeds.Embed(
                description=f"Loaded `{extension}`.",
                colour=discord.Colour.green()))


    @commands.command()
    async def permunload(self, ctx, extension=None):
        """Disables permanently a cog."""
        await ctx.trigger_typing()
        if extension is None:
            return await ctx.send(embed=embeds.Embed(
                description="Please provide a extension. Do `ds!help permunload` for more info",
                colour=self.bot.error_color,
                timestamp=None
            ))

        extension = extension.lower()

        async with aiofiles.open("sebimachine/extensions.txt", 'r') as fp:
            lines = await fp.readlines()

        removed = False
        async with aiofiles.open("sebimachine/extensions.txt", "w") as fp:
            for i in lines:
                if i.strip() != extension:
                    await fp.write(i)
                else:
                    removed = True
                    

        if removed is True:
            try:
                self.bot.unload_extension(extension)
            except:
                pass
            return await ctx.send(embed=embeds.Embed(
                description="Extension successfully unloaded",
                colour=discord.Colour.green()))

        await ctx.send(embed=embeds.Embed(
            description="Extension not found!",
            colour=self.bot.error_color,
            timestamp=None))


def setup(bot):
    bot.add_cog(Contributors(bot))
