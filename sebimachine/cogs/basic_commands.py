#!/usr/bin/python
# -*- coding: utf-8 -*-

from discord.ext import commands
import discord
import asyncio
from sebimachine.utils import utils
from libneko import embeds


class BasicCommands:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(
        aliases=['guide'], 
        brief="Starts a tutorial session that will guide you through this server!")
    async def tutorial(self, ctx):
        """This command will start an interactive guide that can be very easy for newcomers to start learning their way
        in this server!"""
        await ctx.send(embeds=embeds.Embed(
            description=f"Hello, {ctx.author.display_name}. Welcome to Sebi's Bot Tutorials.\n"
            "First off, would you like a quick walkthrough on the server channels?", 
            timestamp=None))
        
        channel_list = {'channel-1'             : self.bot.get_channel(333149949883842561).mention,
                        'd.py-rewrite-start'    : self.bot.get_channel(386419285439938560).mention, 
                        'js-klasa-start'        : self.bot.get_channel(341816240186064897).mention, 
                        'async2rewrite-start'   : self.bot.get_channel(392223495389577217).mention,
                        'd.js'                  : self.bot.get_channel(436771798303113217).mention}

        bots_channels = (self.bot.get_channel(339112602867204097).mention, 
                         self.bot.get_channel(411586546551095296).mention,
                         self.bot.get_channel(431372306217041931).mention)

        help_channels = (self.bot.get_channel(425315253153300488).mention, 
                         self.bot.get_channel(392215236612194305).mention, 
                         self.bot.get_channel(351034776985141250).mention)          

        def check(m):
            return True if m.author.id == ctx.author.id and m.channel.id == ctx.channel.id else False
        
        msg = await self.bot.wait_for(
            'message', 
            check=check,
            timeout=15)

        agree = ("yes", "yep", "non't", "ya", "ye", "yup", "ok", "why not", "yea", "yeah", "hell ye!", "heck ye!",
                 "of course", "ofc", "let's go")

        if msg is None:
            await ctx.send(embeds=embeds.Embed(
                description="Sorry, {ctx.author.mention}, you didn't reply on time. You can run the "
                            "command again when you're free :)",
                timestamp=None))
        else:
            if msg.content.lower() in agree:
                async with ctx.typing():
                    await ctx.send(embeds=embeds.Embed(
                        description="Alrighty-Roo... Check your DMs!", 
                        timestamp=None))
                    await ctx.author.send(embeds=embeds.Embed(
                        description="Alrighty-Roo...", 
                        timestamp=None))
                    
                    await asyncio.sleep(6)
                    await ctx.author.send(embeds=embeds.Embed(
                        description=f"To start making your bot from scratch, you first need to head over to {channel_list['channel-1']}" 
                                      " (Regardless of the language you're gonna use).", 
                        timestamp=None))
                                    
                    await asyncio.sleep(6)
                    await ctx.author.send(embeds=embeds.Embed(
                        description=f"After you have a bot account, you can continue with {channel_list['d.py-rewrite-start']} " 
                                      f"if you want to make a bot in Discord.py Rewrite.\n\n But, if you want to make a bot made with Javascript and you don't like Python, consider using {channel_list['js-klasa-start']} or "
                                      f"{channel_list['d.js']}"
                                      f"\n\n\nIf you already have old Discord.py async code and you want to use it with the new Rewrite versions, head to {channel_list['async2rewrite-start']}",
                        timestamp=None))
                   
                    await asyncio.sleep(8)                 
                    await ctx.author.send(embeds=embeds.Embed(
                        description="\n\n\n...Read all the tutorials and still need help? You have two ways to get help.", 
                                                        timestamp=None))
                    await asyncio.sleep(5)
                    await ctx.author.send(embeds=embeds.Embed(
                        description="**Method-1**\nThis is the best method of getting help. You help yourself.\n"
                                      f"To do so, head over to a bots dedicated channel (either {bots_channels[0]} or {bots_channels[1]}, *and {bots_channels[2]} for testing music features)" 
                                      " and type `?rtfm rewrite thing_you_want_help_with`.\nThis will trigger @R. Danny#6348 and will "
                                      "give you links on your query on the official discord.py rewrite docs. *PS: Let the page completely load to get the auto-scroll*", 
                        timestamp=None))
                                   
                    await asyncio.sleep(10)
                    await ctx.author.send(embeds=embeds.Embed(
                        description="**Method-2**\nIf you haven't found anything useful with the first method, feel free to ask your question "
                                    f"in any of the related help channels. ({', '.join(help_channels)})\nMay the force be with you!!", 
                        timestamp=None))
                                    
            else:
                return await ctx.send(embeds=embeds.Embed(
                    description="Session terminated. You can run this command again whenever you want.", 
                    timestamp=None))

def setup(bot):
    bot.remove_command('help')
    bot.load_extension('libneko.extras.help')
    bot.add_cog(BasicCommands(bot))
