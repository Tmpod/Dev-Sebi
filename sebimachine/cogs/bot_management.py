#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

import discord
from discord.ext import commands

from libneko import embeds

import re


prefix_re = re.compile(r'^\[([^\]]+)\]\s*(.+)$')

RESPONSE_CHANNEL = 459280759945953300
SPAM_CHANNEL = 411586546551095296
TESTING_CHANNEL = 339112602867204097
SBT_GUILD_ID = 265828729970753537
SOME_COLOUR = 0x363941


class BotManager:
    def __init__(self, bot):
        self.bot = bot


    @staticmethod
    async def __local_check(ctx):
        return ctx.guild and ctx.guild.id == SBT_GUILD_ID


    async def on_member_join(self, member):
        # If the member is not a bot
        if not member.bot:
            return

        # Check if the bot is in the db
        existing = await self.bot.db_con.fetchrow('SELECT * FROM bots WHERE id = $1', member.id)
        if not existing:
            return await member.kick(reason="The bot was not on the database")

        bot_owner = member.guild.get_member(await self.bot.db_con.fetchval('SELECT owner FROM bots WHERE id = $1',
                                                                           member.id))
        # Check if the bot is of a staff member and add the moderator role or admin role
        if discord.utils.get(member.guild.roles, name='Administrator') in bot_owner.roles:
            await member.add_roles(discord.utils.get(member.guild.roles, name='Admin Bots'))

        elif discord.utils.get(member.guild.roles, name='Moderator') in bot_owner.roles:
            await member.add_roles(discord.utils.get(member.guild.roles, name='Moderator Bots'))

        # Add roles and change bots nick
        await member.add_roles(discord.utils.get(member.guild.roles, name='Bots'))
        prefix = await self.bot.db_con.fetchval('SELECT prefix FROM bots WHERE id = $1', member.id)
        await member.edit(nick=f'[{prefix}] {member.display_name}')

        # Add role to the owner and send a message to the owner telling that the bot has been added
        await bot_owner.send(embed=embeds.Embed(
            description=f'Your bot, {member.name}, has been added to the server.\n'
                         'You can use it in <#411586546551095296> and <#339112602867204097>.\n'
                         'Enjoy! 😉',
            timestamp=None))
        await bot_owner.add_roles(discord.utils.get(member.guild.roles, name='Bot Developers'))

    async def on_member_remove(self, member):
        if member.bot:
            await self.bot.db_con.execute('DELETE FROM bots WHERE id = $1', member.id)
        else:
            # Kick all of the user's bots
            existing = await self.bot.db_con.fetchrow('SELECT * FROM bots WHERE owner = $1', member.id)
            if not existing:
                # The user has no bots
                return

            f = await self.bot.db_con.fetch("SELECT * FROM bots WHERE owner = $1", member.id)
            values = [i[0] for i in [list(i.values()) for i in f]]

            for v in values:
                bot = member.guild.get_member(v)
                await bot.kick(reason="Owner of this bot left or was kicked")

    async def on_member_ban(self, guild, user):
        if user.bot:
            await self.bot.db_con.execute('DELETE FROM bots WHERE id = $1', user.id)
        else:
            # Ban all of the user's bots
            existing = await self.bot.db_con.fetchrow('SELECT * FROM BOTS WHERE owner = $1', user.id)
            if not existing:
                # The user has no bots
                return

            f = await self.bot.db_con.fetch("SELECT id FROM bots WHERE owner = $1", user.id)
            values = [i[0] for i in [list(i.values()) for i in f]]

            for v in values:
                bot = guild.get_member(v)
                await bot.ban(reason="Owner of this bot was banned")


    @commands.command()
    async def invite(self, ctx, bot=None, prefix=None):
        bot = await ctx.bot.get_user_info(bot)
        if not bot:
            raise Warning('You must include the id of the bot you are trying to invite... Be exact.')
        if not bot.bot:
            raise Warning('You can only invite bots.')
        if not prefix:
            raise Warning('Please provide a prefix.')

        # Make sure that the bot has not been invited already and it is not being tested
        if await self.bot.db_con.fetch('SELECT COUNT (*) FROM bots WHERE id = $1', bot.id) == 1:
            raise Warning('The bot has already been invited or is being tested.')

        await self.bot.db_con.execute('INSERT INTO bots (id, owner, prefix) VALUES ($1, $2, $3)',
                                      bot.id, ctx.author.id, prefix)

        em = embeds.Embed(colour=self.bot.embed_color, timestamp=None)
        em.title = f"Hello {ctx.author.name},"
        em.description = "Thanks for inviting your bot! It will be tested and invited shortly. " \
                         "Please open your DMs if they are not already so the bot can contact " \
                         "you to inform you about the progress of the bot!"
        await ctx.send(embed=em)
                                             
        em = embeds.Embed(
            title="Bot invite",
            colour=SOME_COLOUR)
        em.description = discord.utils.oauth_url(
            bot.id, 
            permissions=None, 
            guild=ctx.guild)
        em.set_thumbnail(url=bot.avatar_url)
        em.add_field(
            name="Bot name", 
            value=bot.name)
        em.add_field(
            name="Bot id", 
            value="`" + str(bot.id) + "`")
        em.add_field(
            name="Bot owner", 
            value=ctx.author.mention)
        em.add_field(
            name="Bot prefix", 
            value="`" + prefix + "`")
        await ctx.bot.get_channel(RESPONSE_CHANNEL).send(embed=em)


    @commands.command(name='claim', aliases=['makemine', 'gimme'])
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def claim_bot(self, ctx, bot: discord.Member = None, prefix: str = None, owner: discord.Member = None):
        if not bot:
            raise Warning('You must include the name of the bot you are trying to claim... be exact!')
        elif not bot.bot:
            raise Warning('You can only claim bots.')
        elif not prefix:
            prefix_match = prefix_re.findall(bot.display_name)
            if prefix_match:
                prefix, name = prefix_match[0]
            else:
                raise Warning('Prefix not provided and can\'t be found in bot nick.')

        if owner is not None and ctx.author.guild_permissions.manage_roles:
            author_id = owner.id
        else:
            author_id = ctx.author.id

        em = embeds.Embed(timestamp=None)

        if await self.bot.db_con.fetchval('SELECT COUNT(*) FROM bots WHERE owner = $1', author_id) >= 10:
            em.colour = self.bot.error_color
            em.title = 'Too Many Bots Claimed'
            em.description = 'Each person is limited to claiming 10 bots as that is how ' \
                             'many bots are allowed by the Discord API per user.'
            return await ctx.send(embed=em)
        existing = await self.bot.db_con.fetchrow('SELECT * FROM bots WHERE id = $1', bot.id)
        if not existing:
            await self.bot.db_con.execute('INSERT INTO bots(id, owner, prefix) VALUES ($1, $2, $3)',
                                          bot.id, author_id, prefix)
            em.colour = self.bot.embed_color
            em.title = 'Bot Claimed'
            em.description = f'You have claimed {bot.display_name} with a prefix of {prefix}\n' \
                             f'If there is an error please run command again to correct the prefix,\n' \
                             f'or {ctx.prefix}unclaim {bot.mention} to unclaim the bot.'
        elif existing['owner'] and existing['owner'] != author_id:
            em.colour = self.bot.error_color
            em.title = 'Bot Already Claimed'
            em.description = 'This bot has already been claimed by someone else.\n' \
                             'If this is actually your bot please let the guild Administrators know.'
        elif existing['owner'] and existing['owner'] == author_id:
            em.colour = self.bot.embed_color
            em.title = 'Bot Already Claimed'
            em.description = 'You have already claimed this bot.\n' \
                             'If the prefix you provided is different from what is already in the database' \
                             ' it will be updated for you.'
            if existing['prefix'] != prefix:
                await self.bot.db_con.execute("UPDATE bots SET prefix = $1 WHERE id = $2", prefix, bot.id)
        elif not existing['owner']:
            await self.bot.db_con.execute('UPDATE bots SET owner = $1, prefix = $2 WHERE id = $3',
                                          author_id, prefix, bot.id)
            em.colour = self.bot.embed_color
            em.title = 'Bot Claimed'
            em.description = f'You have claimed {bot.display_name} with a prefix of {prefix}\n' \
                             f'If there is an error please run command again to correct the prefix,\n' \
                             f'or {ctx.prefix}unclaim {bot.mention} to unclaim the bot.'
        else:
            em.colour = self.bot.error_color
            em.title = 'Something Went Wrong...'

        await ctx.send(embed=em)

    @commands.command(name='unclaim')
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def unclaim_bot(self, ctx, bot: discord.Member = None):
        if not bot:
            raise Warning('You must include the name of the bot you are trying to claim... Be exact.')
        if not bot.bot:
            raise Warning('You can only unclaim bots.')

        em = embeds.Embed(timestamp=None)

        existing = await self.bot.db_con.fetchrow('SELECT * FROM bots WHERE id = $1', bot.id)
        if not existing or not existing['owner']:
            em.colour = self.bot.error_color
            em.title = 'Bot Not Found'
            em.description = 'That bot is not claimed'
        elif existing['owner'] != ctx.author.id and not ctx.author.guild_permissions.manage_roles:
            em.colour = self.bot.error_color
            em.title = 'Not Claimed By You'
            em.description = 'That bot is claimed by someone else.\n' \
                             'You can\'t unclaim someone else\'s bot'
        else:
            await self.bot.db_con.execute('UPDATE bots SET owner = null WHERE id = $1', bot.id)
            em.colour = self.bot.embed_color
            em.title = 'Bot Unclaimed'
            em.description = f'You have unclaimed {bot.display_name}\n' \
                             f'If this is an error please reclaim using\n' \
                             f'{ctx.prefix}claim {bot.mention} {existing["prefix"]}'

        await ctx.send(embed=em)


    @commands.command(name='listclaims', aliases=['claimed', 'mybots'])
    @commands.cooldown(1, 5, commands.BucketType.user)
    async def claimed_bots(self, ctx, usr: discord.Member = None):
        usr = usr or ctx.author

        bots = await self.bot.db_con.fetch('SELECT * FROM bots WHERE owner = $1', usr.id)

        if bots:
            em = embeds.Embed(
                title=f'{usr.display_name} has claimed the following bots:',
                colour=self.bot.embed_color,
                timestamp=None)
            for bot in bots:
                member = ctx.guild.get_member(int(bot['id']))
                em.add_field(
                    name=member.display_name, 
                    value=f'Stored Prefix: {bot["prefix"]}')
        else:
            em = embeds.Embed(
                title='You have not claimed any bots here yet.',
                colour=self.bot.embed_color,
                timestamp=None)
        await ctx.send(embed=em)


    @commands.command(aliases=['whowns', 'owns', 'owner'])
    async def whoowns(self, ctx, bot: discord.Member):
        if not bot.bot:
            return await ctx.send('This command is only for querying bots.')

        owner = await self.bot.db_con.fetchrow('SELECT * FROM bots WHERE id = $1', bot.id)

        if owner is None:
            await ctx.send(embed=embeds.Embed(
                description='No one owns that bot!', 
                colour=self.bot.error_color, 
                timestamp=None))
            await ctx.guild.get_channel(RESPONSE_CHANNEL).send(embed=embeds.Embed(
                description=f'No one appears to own {bot} ({bot.id})',
                colour=self.bot.error_color,
                timestamp=None))
        else:
            await ctx.send(embed=embeds.Embed(
                description=ctx.guild.get_member(owner['owner']).mention, 
                colour=SOME_COLOUR,
                timestamp=None))


def setup(bot):
    bot.add_cog(BotManager(bot))
