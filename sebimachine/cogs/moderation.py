#!/usr/bin/python
# -*- coding: utf-8 -*-

from discord.ext import commands
import discord
from sebimachine.utils.utils import NotStaff


class Moderation:
    """
    Moderation Commands
    """

    def __init__(self, bot):
        self.bot = bot

    def __local_check(self, ctx):
        if discord.utils.get(ctx.guild.roles, id=477417027913515020) in ctx.author.roles:
            return True
        raise NotStaff(f"{ctx.author} is not a staff member!")

    @commands.command()
    async def kick(self, ctx, member: discord.Member = None):
        """
        Kick a discord member from your server.
        Only contributors can use this command.

        Usage:
          - kick <discord.member>

        """
        await ctx.trigger_typing()
        if member is None:
            await ctx.send(embed=discord.Embed(
                description="Are you sure you are capable of this command?",
                timestamp=None))
        try:
            await member.kick()
            await ctx.send(embed=discord.Embed(
                description=f"You kicked **`{member.name}`** from **`{ctx.guild.name}`**"
            ))

        except Exception as e:
            await ctx.send(embed=discord.Embed(
                description="You may not use this command, as you do not have permission to do so:\n\n**`{ctx.guild.name}`**"
                f"\n\n```py\n{e}\n```",
                timestamp=None
            ))

    @commands.command(brief="Bans the specified member")
    async def ban(self, ctx, member: discord.Member = None):
        """
        Ban a discord member from your server.
        Only contributors can use this command.
        """
        await ctx.trigger_typing()
        if member is None:
            await ctx.send(embed=discord.Embed(
                description="Are you sure you are capable of this command?",
                timestamp=None))
        try:
            await member.ban()
            await ctx.send(embed=discord.Embed(
                description=f"You banned **`{member.name}`** from **`{ctx.guild.name}`**"
            ))

        except Exception as e:
            await ctx.send(embed=discord.Embed(
                description="You may not use this command, as you do not have permission to do so:\n\n**`{ctx.guild.name}`**"
                f"\n\n```py\n{e}\n```",
                timestamp=None
            ))


def setup(bot):
    bot.add_cog(Moderation(bot))
