#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-

import discord
from libneko import embeds


class Agreement:
    def __init__(self, bot):
        self.bot = bot
        self.CHANNELS = {'AGREEMENTS_CHANNEL': self.bot.get_channel(478930945609826324),
                         'AGREEMENTS_LOGS': self.bot.get_channel(479037543807975435),
                         }

    async def on_member_join(self, member):
        if member.guild.id != 265828729970753537:
            return

        if member.bot is True:
            return

        await member.add_roles(discord.utils.get(member.guild.roles, name='Not agreed'))

    async def on_message(self, message):
        if message.channel.id != self.CHANNELS['AGREEMENTS_CHANNEL'].id:
            return

        if message.content.lower() != 'i agree':
            await message.delete()
            return await message.author.send(f'You have not typed `I agree` in {self.CHANNELS["AGREEMENTS_CHANNEL"].mention}, instead you typed something else, please make sure you type `I agree`')

        await message.delete()
        await message.author.remove_roles(discord.utils.get(message.guild.roles, name='Not agreed'))
        em = embeds.Embed(description=f'{message.author} has agreed to the rules',
                          colour=discord.Colour(0x363941),
                          )
        await self.CHANNELS["AGREEMENTS_LOGS"].send(embed=em)


def setup(bot):
    bot.add_cog(Agreement(bot))
